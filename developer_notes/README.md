# Developer Notes

A few of the CSS and JS template files have been modified to accommodate the content and request made by the site owner.

## html_modified
Contains original template HTML files with various sections commented out.  
 
Commented out sections were either: 
 - Removed
 - Moved
 - Modified 
 
Some commented sections were modified and removed. When placing sections back in, use modified version.

## original_template
Contains original template files and help doc from the template developer.

## project_templates:
Contains original HTML template files for the various types of project pages.  

When creating a new project page, make a copy of the project page.  

The project pages have been renamed to include "base". (i.e. the original project.html was renamed to projectbase.html)  
## clients_related_pages

The clients page and project pages were created during the initial site creation, but were removed (by request of the site owner) before the site went public. 

To add back, copy clients.html and project folder to the root folder of the project.
