# Developer Notes: Client Related Pages

The clients page and project pages were created during the initial site creation, but were removed (by request of the site owner) before the site went public. 

To add back, copy clients.html and project folder to the root folder of the project.