# GG Forces Website
GG Forces is an international shipping logistics company, whose aim is to highlight their services, past clients, and business philosophy to better market themselves. GG Forces provided a site template created by another software developer, which I modified to meet their needs.  

Followed the template’s established CSS and JavaScript conventions, as well as reduced unnecessary code. To create a better user experience, modified the responsiveness of various elements, added a JavaScript fallback for the site, and added various accessibility features. Documented all modifications.  

April 2018

## Demo
https://www.ggforces.com

## My Role
 - Front-End Development

## Tools/Skills Used
 - HTML5
 - CSS3
 - JavaScript
 - jQuery
